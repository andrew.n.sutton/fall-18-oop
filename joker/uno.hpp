
enum Color
{
  Red,
  Blue,
  Green,
  Yellow,
};

struct Number
{
  Number(int n)
    : val(n)
  {
    assert(0 <= n && n <= 9);
  }
  
  int val;
};

struct NumberCard
{
  Color color;
  Number number;
};

enum Control
{
  Reverse,
  Skip,
};

struct ControlCard
{
  Color color;
  Control ctrl;
};

// Draw 2.
struct DrawCard
{
};

// Can be played on any color.
struct AnyColor
{
};

// Can be played on any color, and draw 4.
struct WildCard
{
};

enum CardKind
{
  number,
  control,
  draw,
  any,
  wild,
};

union CardData
{
  NumberCard num;
  ControlCard ctrl;
  DrawCard draw;
  AnyCard any;
  WildCard wild;
};

struct UnoCard
{
  // Allows conversion from NumberCard to UnoCard.
  UnoCard(NumberCard c)
    : m_kind(number)
  { m_data.num = c; }

  CardKind get_kind() const { return m_kind; }

private:
  CardKind m_kind;
  CardData m_data;
};

void 
print(UnoCard c)
{
  switch (c.get_kind()) {
  case number:
    // print the number
  case control:
    // print the control
  case draw:
    // print the draw card
  case any:
    // print the any card
  case wild:
    // print the wild card
  }
}
