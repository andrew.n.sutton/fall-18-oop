#ifndef CARD_HPP 
#define CARD_HPP

#include <deque>

// 4 bytes for the suit (32 bits)
// Only need 2 bits to represent the value.
// So we waste 30 bits.
enum Suit
{
  Spades, // This is called an enumerator
  Clubs,
  Diamonds,
  Hearts,
};

// 4 bytes for the rank (32 bits)
// We can represent these in 4 bits.
// Currently wasting 28 bits.
enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};
// You can assign integer values to each
// label, but you should think about doing
// that carefully.

enum Color
{
  Black,
  Red,
};

/// Represents a playing card with a suit and a rank.
///
/// This class is represented as an 8-bit value (on
/// any reasonably system). The layout of bits is
/// as follows.
///
///   00ssrrrr
///
/// Where s is a suit bit and r is a rank bit.
class Card
{
public:
  Card() = default;
  // Tells the compiler to make a default
  // constructor that "does the right thing".
  // In this specific case, this will create
  // a card with indeterminate value.

  Card(Rank r, Suit s);
  // Construct a card with a rank and suit.

  // Accessors

  Rank get_rank() const;
  // Returns the rank of the card.

  Suit get_suit() const;
  // Returns the suit of the card.

  Color get_color() const { return (Color)(get_suit() <= Clubs); }
  // Returns the color of the card.

  unsigned char get_raw_value() const { return m_value; }
  /// Returns the underlying representation.

private:
  unsigned char m_value;
};

bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);


std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);
std::ostream& operator<<(std::ostream& os, Card c);


using Deck = std::deque<Card>;

Deck make_deck();


#endif