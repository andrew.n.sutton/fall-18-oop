#include "card.hpp"

#include <iostream>

// The first Card names the class, the
// second names the function (constructor).
Card::Card(Rank r, Suit s)
  : m_value(((unsigned)s << 4) | (unsigned)r)
{
}

Rank
Card::get_rank() const
{
  return (Rank)(m_value & 0b00001111);
}

Suit
Card::get_suit() const
{
  return (Suit)((m_value & 0b00110000) >> 4);
}

// Card::Card()
//   // : m_rank(), m_suit() // What values should we put here?
//   // // If you explicitly invoke the default ctor,
//   // // you get a default value.
// {
// }

std::ostream&
operator<<(std::ostream& os, Rank r)
{
  switch (r) {
  case Ace: return os << "A";
  case Two: return os << "2";
  case Three: return os << "3";
  case Four: return os << "4";
  case Five: return os << "5";
  case Six: return os << "6";
  case Seven: return os << "7";
  case Eight: return os << "8";
  case Nine: return os << "9";
  case Ten: return os << "T";
  case Jack: return os << "J";
  case Queen: return os << "Q";
  case King: return os << "K";
  }
}

std::ostream&
operator<<(std::ostream& os, Suit s)
{
  switch (s)
  {
  case Spades: return os << "\u2660";
  case Clubs: return os << "\u2663";
  case Diamonds: return os << "\u2662";
  case Hearts: return os << "\u2661";
  }
}

bool
operator==(Card a, Card b)
{
  return a.get_raw_value() == b.get_raw_value();
}

bool
operator!=(Card a, Card b)
{
  return !(a == b);
}


bool
operator<(Card a, Card b)
{
  return a.get_raw_value() < b.get_raw_value();
}

bool
operator>(Card a, Card b)
{
  return false;
}

bool
operator<=(Card a, Card b)
{
  return false;

}

bool
operator>=(Card a, Card b)
{
  return false;

}




std::ostream&
operator<<(std::ostream& os, Card c)
{
  return os << c.get_rank() << c.get_suit();
}


Deck
make_deck()
{
  Deck deck {
    {Ace, Spades},
    {Two, Spades},
    {Three, Spades},
    {Four, Spades},
    {Five, Spades},
    {Six, Spades},
    {Seven, Spades},
    {Eight, Spades},
    {Nine, Spades},
    {Ten, Spades},
    {Jack, Spades},
    {Queen, Spades},
    {King, Spades},

    {Ace, Clubs},
    {Two, Clubs},
    {Three, Clubs},
    {Four, Clubs},
    {Five, Clubs},
    {Six, Clubs},
    {Seven, Clubs},
    {Eight, Clubs},
    {Nine, Clubs},
    {Ten, Clubs},
    {Jack, Clubs},
    {Queen, Clubs},
    {King, Clubs},

    {Ace, Diamonds},
    {Two, Diamonds},
    {Three, Diamonds},
    {Four, Diamonds},
    {Five, Diamonds},
    {Six, Diamonds},
    {Seven, Diamonds},
    {Eight, Diamonds},
    {Nine, Diamonds},
    {Ten, Diamonds},
    {Jack, Diamonds},
    {Queen, Diamonds},
    {King, Diamonds},

    {Ace, Hearts},
    {Two, Hearts},
    {Three, Hearts},
    {Four, Hearts},
    {Five, Hearts},
    {Six, Hearts},
    {Seven, Hearts},
    {Eight, Hearts},
    {Nine, Hearts},
    {Ten, Hearts},
    {Jack, Hearts},
    {Queen, Hearts},
    {King, Hearts},
  };
  return deck;

#if 0
  // Gross. member-wise assignment.
  // Card ace_of_spades;
  // ace_of_spades.rank = Ace;
  // ace_of_spades.suit = Spades;
  // Broken when we made members private!

  // Better. Declare each card to have
  // a particular value.
  Card c0 {Ace, Spades};
  Card c1 {Two, Spades};
  Card c2 {Three, Spades};
  Card c3 {Four, Spades};
  Card c4 {Five, Spades};
  // We get to see the actual value of
  // each card. Each initialization is
  // just one line of code.

  // c0.m_rank = Three;
  // Made invalid because it's private.

  Card c;
  // if (predicate)
  //   c = c0;
  // else
  //   c = c1;
  
  // That above is equivalent to this (more or less).
  // Card c = predicate ? c0 : c1;

  

  c = c0;

  return {};
  #endif
}

#if 0
// On equality and substitution...
//
// if x==y then f(x) == f(y)
//
// Use this to reason about (optimize, verify) code.
//
// Here is the slow summation.
int sum(int n)
{
  int ret = 0;
  for (int i = 0; i < n; ++i)
    ret += i;
  return ret;
}

void f() {
  // Suppose this is a separate program.
  // Over time...
  // In version 1, x == 5, f(x) == 15
  // In version 2, y == 5, f(y) == /* something else */
  // Broken!
  sum(5);
}
#endif
