#include "card.hpp"

#include <ctime>
#include <iostream>
#include <random>
#include <algorithm>

int
main()
{
  Deck deck = make_deck();

  std::minstd_rand rng(std::time(nullptr));

  std::shuffle(deck.begin(), deck.end(), rng);

  for (Card c : deck)
    std::cout << c << '\n';

  std::sort(deck.begin(), deck.end());

  std::cout << "-------------------\n";
  for (Card c : deck)
    std::cout << c << '\n';
}