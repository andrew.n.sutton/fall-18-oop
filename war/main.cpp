#include "card.hpp"

#include <ctime>
#include <iostream>
#include <random>
#include <algorithm>

int
main()
{
  // User input processor.
  Rank r;
  while (!(std::cin >> r)) {
    std::cerr << "invalid rank\n";
    std::cin.clear();
  }

#if 0
  Deck deck = make_deck();

  std::minstd_rand rng(std::time(nullptr));

  std::shuffle(deck.begin(), deck.end(), rng);

  // for (Card c : deck)
  //   std::cout << c << '\n';

  std::sort(deck.begin(), deck.end());

  for (Card c : deck)
    std::cout << c << '\n';
#endif
}