#ifndef CARD_HPP 
#define CARD_HPP
// ^- This is a header guard. It prevents
// accidental repeated inclusion of a header
// file.

// #pragma once
// Or just write this. It's equivalent to
// using header guards. Choose this or that.
// Don't use both.

// Source code is organized into physical
// units called components. A component is
// comprised of source files -- usually just
// one header file (.hpp, .hxx, .hh, or .h) 
// and one source file (.cpp, .cxx., .cc, 
// or .C).
//
// The header file contains the *interface*
// of the component. When you #include a header
// file, you are including the interface of
// that component, for your own use.
//
// A header file usually contains:
// - definitions of classes and enums
// - declarations of functions
// - declarations of global variables (rarely!)
// - some other stuff (advanced, maybe!).
//
// The source file contains the 
// *implementation* of the component.
//
// A source file contains:
// - definitions of functions
// - definitions of global variables (if needed)
// - whatever else you want to put there.

// Implementation details must be unreachable
// by "other people" -- can't access or use
// facilities in your implementation. If you
// don't protect implementation facilities,
// those other people will depend on your
// implementation, and if your implementation
// changes, then you potentially break their
// code.

// This is all about good encapsulation!

// Put #includes first in a file.
#include <deque>

enum Suit
{
  Spades, // This is called an enumerator
  Clubs,
  Diamonds,
  Hearts,
};
// Just so you know... an enum is just
// an int. Starts and 0, goes to N - 1
// unless you do something weird.

enum Rank
{
  Ace /* = 1 */,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};
// You can assign integer values to each
// label, but you should think about doing
// that carefully.

enum Color
{
  Black,
  Red,
};

class Card
{
public:
  Card() = default;
  // Tells the compiler to make a default
  // constructor that "does the right thing".
  // In this specific case, this will create
  // a card with indeterminate value.

  Card(Rank r, Suit s);
  // Construct a card with a rank and suit.

  Card(int r, int s)
    : m_rank(static_cast<Rank>(r)),
      m_suit(static_cast<Suit>(s))
  { }

  // Accessors

  Rank get_rank() const { return m_rank; }
  // Returns the rank of the card.

  Suit get_suit() const { return m_suit; }
  // Returns the suit of the card.

  Color get_color() const { return (Color)(m_suit <= Clubs); }
  // Returns the color of the card.

private:
  Rank m_rank;
  Suit m_suit;
};

bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);


std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);
std::ostream& operator<<(std::ostream& os, Card c);

std::istream& operator>>(std::istream& is, Rank& r);
/// Allows textual input using an input stream. 
/// For example:
///
///   Rank r;
///   std::cin >> r;
///
/// The textual representation of a rank is one of
/// the following characters: A, 2, 3, ..., T, J, Q, K.
///

std::istream& operator>>(std::istream& is, Suit& s);

std::istream& operator>>(std::istream& is, Card& c);


// This is the worst of representing a card:
// using Card = int[2];
// Why is the *worst*?
// 1. Array bound errors (e.g., a[2])
// 2. Which element is the suit and which
//    is the rank? Can't tell without docs.
// 3. No constraints on the values of suit
//    and rank (these aren't ints).


using Deck = std::deque<Card>;

Deck make_deck();


#endif