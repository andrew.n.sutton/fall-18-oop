#include "card.hpp"

#include <iostream>

// The first Card names the class, the
// second names the function (constructor).
Card::Card(Rank r, Suit s)
  : m_rank(r), m_suit(s) // member initializers
  // Prefer to initialize members like this.
{
  // // Assign values to members.
  // m_rank = r;
  // m_suit = s;
}

// Card::Card()
//   // : m_rank(), m_suit() // What values should we put here?
//   // // If you explicitly invoke the default ctor,
//   // // you get a default value.
// {
// }

std::ostream&
operator<<(std::ostream& os, Rank r)
{
  switch (r) {
  case Ace: return os << "A";
  case Two: return os << "2";
  case Three: return os << "3";
  case Four: return os << "4";
  case Five: return os << "5";
  case Six: return os << "6";
  case Seven: return os << "7";
  case Eight: return os << "8";
  case Nine: return os << "9";
  case Ten: return os << "T";
  case Jack: return os << "J";
  case Queen: return os << "Q";
  case King: return os << "K";
  }
}

std::ostream&
operator<<(std::ostream& os, Suit s)
{
  switch (s)
  {
  case Spades: return os << "\u2660";
  case Clubs: return os << "\u2663";
  case Diamonds: return os << "\u2662";
  case Hearts: return os << "\u2661";
  }
}

bool
operator==(Card a, Card b)
{
  return a.get_rank() == b.get_rank()
      && a.get_suit() == b.get_suit();
}

bool
operator!=(Card a, Card b)
{
  return !(a == b);
}


bool
operator<(Card a, Card b)
{
  // Product order -- it is not a total order.
  // return a.get_rank() < b.get_rank()
  //     && a.get_suit() < b.get_suit();

  // Lexicographical order.
  if (a.get_suit() < b.get_suit())
    return true;
  if (b.get_suit() < a.get_suit())
    return false;
  return a.get_rank() < b.get_rank();
}

bool
operator>(Card a, Card b)
{
  return false;
}

bool
operator<=(Card a, Card b)
{
  return false;

}

bool
operator>=(Card a, Card b)
{
  return false;

}

std::ostream&
operator<<(std::ostream& os, Card c)
{
  return os << c.get_rank() << c.get_suit();
}

std::istream&
operator>>(std::istream& is, Rank& r)
{
  char c;
  is >> c;
  switch (c) {
  case 'A': r = Ace; break;
  case '2': r = Two; break;
  case '3': r = Three; break;
  case '4': r = Four; break;
  case '5': r = Five; break;
  case '6': r = Six; break;
  case '7': r = Seven; break;
  case '8': r = Eight; break;
  case '9': r = Nine; break;
  case 'T': r = Ten; break;
  case 'J': r = Jack; break;
  case 'Q': r = Queen; break;
  case 'K': r = King; break;
  default:
    // What do we do if the character does not
    // represent a rank (e.g., 'H')?
    //
    // Options:
    //
    // - Do nothing?
    //   Why shouldn't we do this? Caller has no
    //   way to detect (or observe) if the input
    //   is valid. Maybe not ideal (here).

    // - Print an error message and retry?
    //
    //   DO NOT DO THIS.
    //
    //   It is not the responsibility of *this* function
    //   to figure out what to do in error cases. It's
    //   the responsibility of the caller.
    //
    //   Implies that we need to communicate error
    //   state to the caller.

    // - Guess what the user meant? Pick a default value?
    //
    //   DO NOT DO THIS
    //
    //   Not an error condition. We can't tell if the
    //   input contained a valid value or not.
  
    // - Exit the program (with error code)?
    //   
    //   Implies that your program has run to completion.
    //   The exit code is used to communicate with the
    //   program that runs this program, not to indicate
    //   an error within this program.
    //
    //   A bit extreme, but not the worst thing you can do.
    //
    // std::exit(EXIT_FAILURE);
  
    // - Abort the program?
    //
    // Also a bit extreme. BUT, this gives us the 
    // opportunity to debug the program.
    //
    // Use abort to stop the program when a logic error
    // is detected. This useful for debugging but
    // *rarely* production systems.
    //
    // std::abort();
  
    // - Throw an exception.
    //
    // This is not bad. Borderline good.
    //
    // Usually exceptions are thrown for exceptional
    // behaviors (input). Somebody, somewhere has to
    // deal with exceptions. These are often used for
    // "system is going down" kinds of errors. They
    // give the system a chance to respond to a critical
    // failure of some kind.
    //
    // throw std::runtime_error("invalid rank");
  
    // - "Return" an error code.
    //
    // This is a GOOD strategy for validating input
    // user input. Makes the caller of this function
    // responsible for processing the error.
    //
    // Sets the fail bit for the istream.
    is.setstate(std::ios::failbit);
    break;
  }
  return is;
}

std::istream&
operator>>(std::istream& is, Suit& s)
{
  return is;
}

std::istream&
operator>>(std::istream& is, Card& c)
{
  return is;
}





Deck
make_deck()
{
  Deck deck {
    {Ace, Spades},
    {Two, Spades},
    {Three, Spades},
    {Four, Spades},
    {Five, Spades},
    {Six, Spades},
    {Seven, Spades},
    {Eight, Spades},
    {Nine, Spades},
    {Ten, Spades},
    {Jack, Spades},
    {Queen, Spades},
    {King, Spades},

    {Ace, Clubs},
    {Two, Clubs},
    {Three, Clubs},
    {Four, Clubs},
    {Five, Clubs},
    {Six, Clubs},
    {Seven, Clubs},
    {Eight, Clubs},
    {Nine, Clubs},
    {Ten, Clubs},
    {Jack, Clubs},
    {Queen, Clubs},
    {King, Clubs},

    {Ace, Diamonds},
    {Two, Diamonds},
    {Three, Diamonds},
    {Four, Diamonds},
    {Five, Diamonds},
    {Six, Diamonds},
    {Seven, Diamonds},
    {Eight, Diamonds},
    {Nine, Diamonds},
    {Ten, Diamonds},
    {Jack, Diamonds},
    {Queen, Diamonds},
    {King, Diamonds},

    {Ace, Hearts},
    {Two, Hearts},
    {Three, Hearts},
    {Four, Hearts},
    {Five, Hearts},
    {Six, Hearts},
    {Seven, Hearts},
    {Eight, Hearts},
    {Nine, Hearts},
    {Ten, Hearts},
    {Jack, Hearts},
    {Queen, Hearts},
    {King, Hearts},
  };
  return deck;

#if 0
  // Gross. member-wise assignment.
  // Card ace_of_spades;
  // ace_of_spades.rank = Ace;
  // ace_of_spades.suit = Spades;
  // Broken when we made members private!

  // Better. Declare each card to have
  // a particular value.
  Card c0 {Ace, Spades};
  Card c1 {Two, Spades};
  Card c2 {Three, Spades};
  Card c3 {Four, Spades};
  Card c4 {Five, Spades};
  // We get to see the actual value of
  // each card. Each initialization is
  // just one line of code.

  // c0.m_rank = Three;
  // Made invalid because it's private.

  Card c;
  // if (predicate)
  //   c = c0;
  // else
  //   c = c1;
  
  // That above is equivalent to this (more or less).
  // Card c = predicate ? c0 : c1;

  

  c = c0;

  return {};
  #endif
}

#if 0
// On equality and substitution...
//
// if x==y then f(x) == f(y)
//
// Use this to reason about (optimize, verify) code.
//
// Here is the slow summation.
int sum(int n)
{
  int ret = 0;
  for (int i = 0; i < n; ++i)
    ret += i;
  return ret;
}

void f() {
  // Suppose this is a separate program.
  // Over time...
  // In version 1, x == 5, f(x) == 15
  // In version 2, y == 5, f(y) == /* something else */
  // Broken!
  sum(5);
}
#endif
