
#include "card.hpp"

#include <iostream>


void 
print(PlayingCard c)
{
  if (c.is_suited())
    std::cout << c.get_suited() << '\n';
  else
    std::cout << c.get_joker() << '\n';
}

int
main() 
{
  PlayingCard sa {Card{Ace, Spades}};
  PlayingCard s2 {Card{Two, Spades}};
  PlayingCard jr {Joker{Red}};
  print(sa); // OK
  print(s2); // OK?
  print(jr); // OK?

  // SuitCard c1 = SuitCard::from_raw_value(0);
  // SuitCard c2 = c1.from_raw_value(0);

  // SuitCard c3 = 0;
  // SuitCard c3(0);


}
