#pragma once

#include <iosfwd>
#include <cassert>

enum Suit
{
  Spades,
  Clubs,
  Diamonds,
  Hearts,
};

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Color
{
  Black,
  Red,
};


/// A suit card has one of the four suits above and an associated rank.
/// The suit and rank value is represented as an 8-bit integer with the
/// following bit pattern:
///
///   00ssrrrr
///
/// where s denotes a bit used to represent the suit and r denotes a
/// bit to represent the rank. The high-order bits are zero.
class SuitCard 
{
private:
  explicit SuitCard(unsigned n);    
  /// Constructs a suited card from an integer `n` after masking off the
  /// two high-order bits. This ensures that `m_value` matches the pattern
  /// above. Behavior is undefined if the rank or suit would be invalid.

public:
  SuitCard(Rank r, Suit s);
  /// Constructs a card with the given rank and suit (e.g., Ace of Spades).

  Rank get_rank() const;
  /// Returns the rank of the card.

  Suit get_suit() const;
  /// Returns the suit of the card.

  Color get_color() const;
  /// Returns the color of the suit.

  unsigned to_raw_value() const;
  /// Returns the underlying representation of this card.

  static SuitCard from_raw_value(unsigned value);
  /// Returns a suited card constructed from an uninterpreted value.
  ///
  /// This is sometimes called a "factory method" because
  /// it creates and returns an object. This is also sometimes
  /// called a "named constructor" because it returns an
  /// object of this class.

private:
  unsigned char m_value;
};

inline
SuitCard::SuitCard(unsigned n)
  : m_value(n & 0x3f)
{
  // Check that the rank is valid.
  assert((n & 0x0f) <= King);
}

inline
SuitCard::SuitCard(Rank r, Suit s)
  : m_value((static_cast<unsigned>(s) << 4) | static_cast<unsigned>(r))
{ }

inline Suit
SuitCard::get_suit() const
{
  return static_cast<Suit>((m_value & 0x30) >> 4);
}

inline Rank
SuitCard::get_rank() const
{
  return static_cast<Rank>(m_value & 0x0f);
}

inline Color
SuitCard::get_color() const
{
  // Note that get_suit() <= Clubs returns true (1) if the suit is
  // diamonds or hearts, and false (0) otherwise. Those integer values
  // correspond to the colors Red and Black.
  return static_cast<Color>(get_suit() >= Diamonds);
}

inline unsigned
SuitCard::to_raw_value() const
{
  return m_value;
}

inline SuitCard
SuitCard::from_raw_value(unsigned value)
{
  return SuitCard(value);
}


/// Represents one of the two jokers in a standard deck. A joker is
/// either red or black (i.e., it has a color). The value of a joker
/// is represented as an 8-bit integer with the following bit pattern.
///
///     0000000c
///
/// In other words, the color bit is stored in the low-order bit of
/// the value.
class JokerCard
{
  explicit JokerCard(unsigned n);    
  /// Constructs a joker from an integer `n` after masking off all but
  /// two low-order bit. This ensures that `m_value` matches the pattern
  /// above.

public:
  JokerCard(Color c);
  /// Constructs a joker with the given color.

  Color get_color() const { return m_color; }
  /// Returns the color of the card.

  unsigned to_raw_value() const;
  /// Returns the underlying representation of the card.

  static JokerCard from_raw_value(unsigned value);
  /// Returns a joker constructed from the given value.

private:
  unsigned char m_value;
};

inline
JokerCard::JokerCard(unsigned n)
  : m_value(n & 0x01)
{ }

inline
JokerCard::JokerCard(Color c)
  : m_value(static_cast<unsigned>(c))
{ }

inline Color
JokerCard::get_color()
{
  return static_cast<Color>(m_value());
}

inline unsigned
JokerCard::to_raw_value() const
{
  return m_value;
}

inline JokerCard
JokerCard::from_raw_value(unsigned value)
{
  return JokerCard(value);
}


/// A playing card is either suited or a joker. This set of values is
/// represented as an 8-bit integer having one of the following bit pattern:
///
///     00rrssss
///     1000000c
///
/// The high order bit is used to determine whether the value represents
/// a suited card or a joker.
class PlayingCard
{
public:
  PlayingCard(SuitCard c);
  /// Constructs a suited card.
  
  PlayingCard(Rank r, Suit s);
  /// Constructs a suited card with rank `r` and suit `s`.

  PlayingCard(JokerCard j);
  /// Constructs a joker.

  PlayingCard(Color c);
  /// Constructs a joker with color `c`.

  // Queries

  bool is_suited() const;
  // True if this card is suited.

  bool is_joker() const;
  // True if this is a joker.

  SuitCard get_suited() const;
  /// Returns this as a suited card. Behavior is undefined if `!is_suited()`.

  JokerCard get_joker() const;
  /// Returns this as a joker. Behavior is undefined if `!is_joker()`.

private:
  unsigned char m_value;
};

inline
PlayingCard::PlayingCard(SuitCard c)
  : m_value(c.to_raw_value())
{ }

inline
PlayingCard::PlayingCard(Rank r, Suit s)
  : PlayingCard(SuitCard(r, s))
{ }

inline
PlayingCard::PlayingCard(JokerCard c)
  : m_value(0x80 | c.to_raw_value())
{ }

inline
PlayingCard::PlayingCard(Color c)
  : PlayingCard(JokerCard(c))
{ }

inline bool
PlayingCard::is_suited() const
{
  return !(m_value & 0x80);
}

inline bool
PlayingCard::is_joker() const
{
  return m_value & 0x80;
}

inline SuitCard
PlayingCard::get_suited() const
{
  return SuitCard::from_raw_value(m_value);
}

inline JokerCard
PlayingCard::get_joker() const
{
  return JokerCard::from_raw_value(m_value);
}


std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);
std::ostream& operator<<(std::ostream& os, Color c);
std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Joker j);
