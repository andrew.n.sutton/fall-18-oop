
#include "card.hpp"

/// Represents the value of a hand whose value is a high card. Note
/// that high card values are compared lexicographically. This value
/// is represented as a sorted sequence of five 4-bit values in a 
/// 32-bit integer such that the lowest ranked card is stored in the
/// low-order bits of  the integer. In other words, the value has this
/// hex pattern:
///
///     00 01 23 45
///
/// Where the digits represent the position of cards in order of their
/// rank.
struct HighValue
{
public:
  HighValue(std::initializer_list<Rank> cards);
  /// Constructs a high-card value from the sorted sequence of ranks.

  Rank operator[](int n) const;
  /// Returns the nth high card in the sequence s.t., 0 yields the highest
  /// ranked card.

private:
  unsigned m_value;
};

inline 
HighValue::HighValue(std::initializer_list<Rank> cards)
  m_value((static_cast<unsigned>(card[0]) << 16) |
          (static_cast<unsigned>(card[1]) << 12) |
          (static_cast<unsigned>(card[2]) << 8) |
          (static_cast<unsigned>(card[3]) << 4) |
          (static_cast<unsigned>(card[4]) << 0))
{
  assert(cards.size() == 5);
  assert(std::is_sorted(cards.begin(), cards.end()));
}

bool operator<(High a, High b)
{
  return a.ranks < b.ranks;
}

struct Two
{
  // The rank of the pair.
  Rank pair;

  // The sorted rank of remaining cards.
  std::array<Rank, 3> high;
};

struct TwoPair
{
  // The sorted ranks of pairs.
  std::array<Rank, 2> pairs;

  // The remaining high card.
  Rank high;
};

struct Three
{
  // The rank of the three-of-a-kind.
  Rank three;

  // Sorted high cards for tie breakers.
  std::array<Rank, 2>;
};

struct Straight
{
  // The rank of the highest card.
  Rank high;
};

struct Flush
{
  // High card for equal flushes wins.
  std::array<Rank, 5>;
};

struct FullHouse
{
  // Two ranks: the pair over the triple.
  std::array<Rank, 2>;
};

struct Four
{
  Rank rank;

  // NOTE: Not usually needed since you can't
  // have two equal four-of-a-kind values.
  Rank high;
};

struct StraightFlush
{
  // Just need the rank of the high card.
  Rank high;
};

// Discriminator for the values of a poker hand.
enum ValueKind
{
  high,
  two_kind,
  two_pair,
  three_kind,
  straight,
  flush,
  full_house,
  four_kind,
  straight_flush,
};

enum ValueData
{
  High high;
  Two two;
  TwoPair twice;
  Three three;
  Straight straight;
  Flush flush;
  FullHouse house;
  Four four;
  StraightFlush sf;
};

/// Represents the value of a poker hand.
///
/// The value is represented in a 32-bit bitfield
/// using one of the following layouts:
///
///     00 dv vv vv
///
/// FIXME: Implement this.
class Value
{
  unsigned m_value;
};

bool operator<(const Value& a, const Value& b)
{
  if (a.k < b.k)
    return true;
  if (b.k < a.k)
    return false;
  switch (a.k) {
  case high:
    return a.high < b.high;
  case two_kind:
  case two_pair:
  case three_kind:
  case straight:
  case flush:
  case full_house:
  case four_kind:
  case straight_flush:
  }
}
