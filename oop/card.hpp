#pragma once

#include <iostream>

enum Suit
{
  Spades, 
  Clubs,
  Diamonds,
  Hearts,
};

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Color
{
  Black,
  Red,
  Magic,
};


enum Kind
{
  suited,
  joker
};

// Card is a base class.
struct Card
{
  Card(Kind k) : kind(k) { }
  
  Kind kind;
};


struct SuitCard : Card 
  // SuitCard inherits from Card.
  // SuitCard derives from Card.
  // SuitCard is a derived class.
{
  // Inheritance means that SuitCard "gets"
  // the members of its base class(es).

  SuitCard (Rank r, Suit s) 
    : Card(suited), rank(r), suit(s)
  { }
  
  Rank rank;
  Suit suit;
};

struct JokerCard : Card
  // JokerCard inherits from Card.
{
  JokerCard(Color c) 
    : Card(joker), color(c) { }
  
  Color color;
};
