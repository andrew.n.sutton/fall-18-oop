
#include "card.hpp"

#include <iostream>

// NOTE: Accepts a REFERENCE to a card.
void print(Card& c)
{
  // We can talk about two different types of
  // the object referred to by c.
  //
  // The STATIC TYPE of c is what's written.
  // Here, c looks like a Card object.
  //
  // The DYNAMIC TYPE of c is the type of the
  // object referred to.
  //
  // std::cerr << c.kind << '\n';
  // std::cerr << c.suit << '\n';

  if (c.kind == suited) {
    SuitCard& sc = static_cast<SuitCard&>(c);
    std::cout << sc.rank << ' ' << sc.suit << '\n';
  } else {
    JokerCard& jc = static_cast<JokerCard&>(c);
    std::cout << jc.color << '\n';
  }
}

// You MUST pass base classes by reference. If
// you don't, you slice off the dynamic type of
// the object.
//
// You MUST PASS BY REFERENCE.
// You MUST PASS BY REFERENCE.
// You MUST PASS BY REFERENCE.
// You MUST PASS BY REFERENCE.
// You MUST PASS BY REFERENCE.
// You MUST PASS BY REFERENCE.
//
// In C++, with inheritance.
//
void slice(Card c)
{
  // What is the static type of c?
  // What is the dynamic type of c?
  // std::cout << c.kind << '\n';

  if (c.kind == suited) {
    SuitCard& sc = static_cast<SuitCard&>(c);
    std::cout << sc.rank << ' ' << sc.suit << '\n';
  } else {
    JokerCard& jc = static_cast<JokerCard&>(c);
    std::cout << jc.color << '\n';
  }
}

int
main()
{
  SuitCard sc {Queen, Hearts};
  // std::cout << sc.rank << '\n';
  // std::cout << sc.suit << '\n';
  // std::cout << sc.color << '\n';
  // sc.hello();
  print(sc); // #1
  slice(sc);

  JokerCard r {Red};
  // std::cout << r.color << '\n';
  // std::cout << r.Card::color << '\n';
  // r.hello();
  print(r); // #2
  slice(r);

  Card& r1 = sc; // OK: upcast on references
  // SuitCard& r2 = r1; // error: can't downcast

  Card* p1 = &sc; // OK: upcast on pointers
  // SuitCard* p2 = p1;  // error: can't downcast
}
