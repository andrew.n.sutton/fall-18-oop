#pragma once

#include <iostream>

/// Represents the set of all expressions
/// in this tiny little language.
class Expr
{
public:
  virtual ~Expr() = default;
  /// Ensures proper cleanup.

  virtual int eval() const = 0;
  // Evaluates this expression and returns its
  // integer value.
  //
  // A virtual function with "=0" as its definition
  // is a *pure virtual function*. Pure virtual
  // functions must be overridden in a derived class.

  virtual void print(std::ostream& os) const = 0;
  /// Prints this expression to os (in "normal math"
  /// notation).

  void whee() { std::cout << "whee"; }
  // Prints "whee".
};

class Int : public Expr
{
public:
  Int(int n) : value(n) { }
  
  virtual int eval() const override {
    return value;
  }

  void print(std::ostream& os) const override {
    // Prints the integer value.
    // os << value;

    os << "push " << value << '\n';
  }

  int value;
};

// A binary expression has two operands
struct Binary : Expr
{
  Binary(Expr *e1, Expr* e2)
    : lhs(e1), rhs(e2)
  { }

protected:
  void print_binary(std::ostream& os, char op) const
  {
    // "Normal math notation -- infix".
    // os << '(';
    // lhs->print(os);
    // os << op;
    // rhs->print(os);
    // os << ')';

    // // Prefix notation.
    // os << '(';
    // os << op << ' ';
    // lhs->print(os);
    // os << ' ';
    // rhs->print(os);
    // os << ')';

    // // Postfix notation (allows stack-based eval)
    // lhs->print(os);
    // os << ' ';
    // rhs->print(os);
    // os << ' ';
    // os << op;

    // Postfix notation (allows stack-based eval)
    lhs->print(os);
    rhs->print(os);
    switch (op) {
    case '+':
      os << "add\n"; break;
    case '-':
      os << "sub\n"; break;
    case '*':
      os << "mul\n"; break;
    case '/':
      os << "div\n"; break;
    case '%':
      os << "rem\n"; break;
    default:
      assert(false);
    }
  }

  // NOTE: Binary does not override eval() or
  // or print(). That's okay... derived classes
  // will do it.

  Expr *lhs; // Left hand side
  Expr *rhs; // Right hand side
};

/// Represents the expression e1 + e2.
struct Add : Binary
{
  // Inherited constructor.
  using Binary::Binary;

  int eval() const override {
    return lhs->eval() + rhs->eval();
  }

  void print(std::ostream& os) const override {
    print_binary(os, '+');
  }

};

struct Sub : Binary
{
  using Binary::Binary;

  int eval() const override {
    int a = lhs->eval();
    int b = rhs->eval();
    return a - b;
  }

  void print(std::ostream& os) const override {
    print_binary(os, '-');
  }
};

struct Mul : Binary
{
  using Binary::Binary;

  int eval() const override {
    int a = lhs->eval();
    int b = rhs->eval();
    return a * b;
  }

  void print(std::ostream& os) const override {
    print_binary(os, '*');
  }
};

struct Div : Binary
{
  using Binary::Binary;

  int eval() const override {
    int a = lhs->eval();
    int b = rhs->eval();
    return a / b;
  }

  void print(std::ostream& os) const override {
    print_binary(os, '/');
  }
};

struct Rem : Binary
{
  using Binary::Binary;

  int eval() const override {
    return lhs->eval() % rhs->eval();
  }

  void print(std::ostream& os) const override {
    print_binary(os, '%');
  }
};

struct Exp : Binary
{
  using Binary::Binary;

  int eval() const override {
    return 0;
  }
};

struct Unary : Expr
{
  Expr *arg; // The operand
};

// Represents expressions of the form -e.
struct Neg : Unary
{
  int eval() const override {
    return -arg->eval();
  }

  void print(std::ostream& os) const override {
    os << '-';
    arg->print(os);
  }
};

// Represents expressions of the form +e.
struct Pos : Unary
{
  int eval() const override {
    return +arg->eval();
  }

  void print(std::ostream& os) const override {
    os << '+';
    arg->print(os);
  }
};
