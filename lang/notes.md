
# Various notes

Member functions of classes

- return values of private data members
- compute associate values
- update the state of the class (e.g., insert into
  a vector, set, map, tree, list, whatever).

Generally, we associate behaviors directly
with objects of that class.

Virtual member functions: same as above, except
that behavior can be overridden for derived
classes.

Other terms that show up (in pairs):

- Early binding/late binding -- early is for
non-virtual functions, late is for virtual functions.
- Static dispatch/dynamic dispatch -- static is
for non-virtual, dynamic is for virtual.

A *polymorphic class* is one with at least one virtual
function (declared or inherited).

An *abstract class* is one with at least one pure 
virtual function (declared or inherited).


Access on base class specifiers (base classes)

Public inheritance says that public members of the
base class are inherited publicly. Derived class objects
can be used interchangeably with base class objects
(i.e., conversions are allowed). NOTE: Substitution
is allowed.

Public inheritance is sometimes called subtype inheritance.
Goal is to establish the "is-a" relationship.

Private inheritance says that public members of
the base class are inherited privately. Derived class
objects cannot be used interchangeably with base
class objects (i.e., no conversions). NOTE: Substitution
is not allowed.

Private inheritance allows you to "inherit" capabilities,
but do not allow substitutability.


Sometimes called implementation inheritance.



