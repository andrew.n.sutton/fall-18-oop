#include "lang.hpp"

#include <iostream>

std::ostream&
operator<<(std::ostream& os, Expr const* e)
{
  e->print(os);
  return os;
}

struct Base
{
  void foo() { }
};

struct Public : Base { }; // Public inheritance
class  Private : Base { }; // Private inheritance


int
main()
{
  Public pub;
  Private priv;

  // Conversion is allowed because of public inheritance.
  Base* p0 = &pub;
  pub.foo(); // OK
  p0->foo(); // OK

  // Conversion is not allowed because of private inheritance.
  // Base* p1 = &priv
  // priv.foo();


  // Expr e0;
  // Binary e1;
  // new Exp(nullptr, nullptr):

  // 3 * 4 - 2 
  // 3 * (4 - 2)
  Expr* e = 
    new Mul(
      new Int(3),
      new Sub(
        new Int(4),
        new Int(2)));

  // Binary* p = static_cast<Binary*>(e);
  // p->print_binary(std::cout, '?');

  std::cout << e << '\n';

  std::cout << "value: " << e->eval() << '\n';
}
