
#include <iostream>

struct Base;
struct Derived;

// Does not work.

void Base_f(Base* p) { std::cout << "B\n";}

struct Base
{
  void f() { Base_f(this); }

  void g() = 0;
};

void Derived_g(Derived* p) { std::cout << "D\n"; }

struct Derived : Base
{
  void g() { Derived_g(this); }
};

void foo(Base& p) {
  p.f();
  p.g();
}

int main()
{
  Derived d;
  foo(d);
}
