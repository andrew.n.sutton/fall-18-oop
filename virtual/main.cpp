
struct S
{
  // You write this.
  void f() { }

  // You write this:
  void f() const { }
};

// The compiler reasons about this.
void f(S* ptr) { }

void f(S const* ptr) { }

int
main()
{
  S x;
  x.f();
  // f(x);
}


