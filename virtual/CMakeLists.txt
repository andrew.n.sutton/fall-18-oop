
set(CMAKE_CXX_FLAGS "-std=c++14")

add_executable(main main.cpp)
add_executable(virtual virtual.cpp)
add_executable(virtual-try2 virtual-try2.cpp)
add_executable(virtual-table virtual_table.cpp)
