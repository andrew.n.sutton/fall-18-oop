; ModuleID = 'main.cpp'
source_filename = "main.cpp"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

%struct.S = type { i8 }

; Function Attrs: nounwind ssp uwtable
define void @_Z1fP1S(%struct.S* %ptr) #0 {
entry:
  %ptr.addr = alloca %struct.S*, align 8
  store %struct.S* %ptr, %struct.S** %ptr.addr, align 8
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @_Z1fPK1S(%struct.S* %ptr) #0 {
entry:
  %ptr.addr = alloca %struct.S*, align 8
  store %struct.S* %ptr, %struct.S** %ptr.addr, align 8
  ret void
}

; Function Attrs: norecurse ssp uwtable
define i32 @main() #1 {
entry:
  %x = alloca %struct.S, align 1
  call void @_ZN1S1fEv(%struct.S* %x)
  ret i32 0
}

; Function Attrs: nounwind ssp uwtable
define linkonce_odr void @_ZN1S1fEv(%struct.S* %this) #0 align 2 {
entry:
  %this.addr = alloca %struct.S*, align 8
  store %struct.S* %this, %struct.S** %this.addr, align 8
  %this1 = load %struct.S*, %struct.S** %this.addr, align 8
  ret void
}

attributes #0 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { norecurse ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 4.0.0 (git@github.com:asutton/clang-reflect.git 8317e40b61d41300797ead958d7b7c9a13bed863) (git@github.com:llvm-mirror/llvm.git 650050c7c2061ec08da359b57e12ae7a4b8c8911)"}
