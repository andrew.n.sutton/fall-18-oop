
// #include <iostream>

struct Base
{
  Base() {
    f();
    // g(); -- Don't call. Will crash.
    h(); // Calls Base::h
  }

  ~Base()
  {
    f(); // Base::f
    h(); // Calls base::h
    g(); // Will crash
  }

  virtual void f() { 
    // std::cout << "Base::f\n"; 
  }

  virtual void g() = 0;

  virtual void h() { 
    // std::cout << "Base::h\n"; 
  }
};

struct Derived : Base
{
  void g() override { 
    // std::cout << "D\n"; 
  }

  void h() override { 
    // std::cout << "D::h\n"; 
  }
};

void foo(Base& p) {
  p.f();
  p.g();
}

int main()
{
  Derived d;
  foo(d);
}
