; ModuleID = 'virtual.cpp'
source_filename = "virtual.cpp"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

%struct.Base = type { i32 (...)** }
%struct.Derived = type { %struct.Base }

@_ZTV7Derived = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI7Derived to i8*), i8* bitcast (void (%struct.Base*)* @_ZN4Base1fEv to i8*), i8* bitcast (void (%struct.Derived*)* @_ZN7Derived1gEv to i8*), i8* bitcast (void (%struct.Derived*)* @_ZN7Derived1hEv to i8*)], align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS7Derived = linkonce_odr constant [9 x i8] c"7Derived\00"
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS4Base = linkonce_odr constant [6 x i8] c"4Base\00"
@_ZTI4Base = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @_ZTS4Base, i32 0, i32 0) }
@_ZTI7Derived = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @_ZTS7Derived, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI4Base to i8*) }
@_ZTV4Base = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI4Base to i8*), i8* bitcast (void (%struct.Base*)* @_ZN4Base1fEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%struct.Base*)* @_ZN4Base1hEv to i8*)], align 8

; Function Attrs: ssp uwtable
define void @_Z3fooR4Base(%struct.Base* dereferenceable(8) %p) #0 {
entry:
  %p.addr = alloca %struct.Base*, align 8
  store %struct.Base* %p, %struct.Base** %p.addr, align 8
  %0 = load %struct.Base*, %struct.Base** %p.addr, align 8
  %1 = bitcast %struct.Base* %0 to void (%struct.Base*)***
  %vtable = load void (%struct.Base*)**, void (%struct.Base*)*** %1, align 8
  %vfn = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable, i64 0
  %2 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn, align 8
  call void %2(%struct.Base* %0)
  %3 = load %struct.Base*, %struct.Base** %p.addr, align 8
  %4 = bitcast %struct.Base* %3 to void (%struct.Base*)***
  %vtable1 = load void (%struct.Base*)**, void (%struct.Base*)*** %4, align 8
  %vfn2 = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable1, i64 1
  %5 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn2, align 8
  call void %5(%struct.Base* %3)
  ret void
}

; Function Attrs: norecurse ssp uwtable
define i32 @main() #1 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
entry:
  %d = alloca %struct.Derived, align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  call void @_ZN7DerivedC1Ev(%struct.Derived* %d)
  %0 = bitcast %struct.Derived* %d to %struct.Base*
  invoke void @_Z3fooR4Base(%struct.Base* dereferenceable(8) %0)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZN7DerivedD1Ev(%struct.Derived* %d)
  ret i32 0

lpad:                                             ; preds = %entry
  %1 = landingpad { i8*, i32 }
          cleanup
  %2 = extractvalue { i8*, i32 } %1, 0
  store i8* %2, i8** %exn.slot, align 8
  %3 = extractvalue { i8*, i32 } %1, 1
  store i32 %3, i32* %ehselector.slot, align 4
  invoke void @_ZN7DerivedD1Ev(%struct.Derived* %d)
          to label %invoke.cont1 unwind label %terminate.lpad

invoke.cont1:                                     ; preds = %lpad
  br label %eh.resume

eh.resume:                                        ; preds = %invoke.cont1
  %exn = load i8*, i8** %exn.slot, align 8
  %sel = load i32, i32* %ehselector.slot, align 4
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val2

terminate.lpad:                                   ; preds = %lpad
  %4 = landingpad { i8*, i32 }
          catch i8* null
  %5 = extractvalue { i8*, i32 } %4, 0
  call void @__clang_call_terminate(i8* %5) #5
  unreachable
}

; Function Attrs: inlinehint ssp uwtable
define linkonce_odr void @_ZN7DerivedC1Ev(%struct.Derived* %this) unnamed_addr #2 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  call void @_ZN7DerivedC2Ev(%struct.Derived* %this1)
  ret void
}

declare i32 @__gxx_personality_v0(...)

; Function Attrs: inlinehint ssp uwtable
define linkonce_odr void @_ZN7DerivedD1Ev(%struct.Derived* %this) unnamed_addr #2 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  call void @_ZN7DerivedD2Ev(%struct.Derived* %this1)
  ret void
}

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) #3 {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #6
  call void @_ZSt9terminatev() #5
  unreachable
}

declare i8* @__cxa_begin_catch(i8*)

declare void @_ZSt9terminatev()

; Function Attrs: inlinehint ssp uwtable
define linkonce_odr void @_ZN7DerivedC2Ev(%struct.Derived* %this) unnamed_addr #2 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  %0 = bitcast %struct.Derived* %this1 to %struct.Base*
  call void @_ZN4BaseC2Ev(%struct.Base* %0)
  %1 = bitcast %struct.Derived* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV7Derived, i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 8
  ret void
}

; Function Attrs: ssp uwtable
define linkonce_odr void @_ZN4BaseC2Ev(%struct.Base* %this) unnamed_addr #0 align 2 {
entry:
  %this.addr = alloca %struct.Base*, align 8
  store %struct.Base* %this, %struct.Base** %this.addr, align 8
  %this1 = load %struct.Base*, %struct.Base** %this.addr, align 8
  %0 = bitcast %struct.Base* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV4Base, i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 8
  %1 = bitcast %struct.Base* %this1 to void (%struct.Base*)***
  %vtable = load void (%struct.Base*)**, void (%struct.Base*)*** %1, align 8
  %vfn = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable, i64 0
  %2 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn, align 8
  call void %2(%struct.Base* %this1)
  %3 = bitcast %struct.Base* %this1 to void (%struct.Base*)***
  %vtable2 = load void (%struct.Base*)**, void (%struct.Base*)*** %3, align 8
  %vfn3 = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable2, i64 2
  %4 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn3, align 8
  call void %4(%struct.Base* %this1)
  ret void
}

; Function Attrs: nounwind ssp uwtable
define linkonce_odr void @_ZN4Base1fEv(%struct.Base* %this) unnamed_addr #4 align 2 {
entry:
  %this.addr = alloca %struct.Base*, align 8
  store %struct.Base* %this, %struct.Base** %this.addr, align 8
  %this1 = load %struct.Base*, %struct.Base** %this.addr, align 8
  ret void
}

; Function Attrs: nounwind ssp uwtable
define linkonce_odr void @_ZN7Derived1gEv(%struct.Derived* %this) unnamed_addr #4 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  ret void
}

; Function Attrs: nounwind ssp uwtable
define linkonce_odr void @_ZN7Derived1hEv(%struct.Derived* %this) unnamed_addr #4 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind ssp uwtable
define linkonce_odr void @_ZN4Base1hEv(%struct.Base* %this) unnamed_addr #4 align 2 {
entry:
  %this.addr = alloca %struct.Base*, align 8
  store %struct.Base* %this, %struct.Base** %this.addr, align 8
  %this1 = load %struct.Base*, %struct.Base** %this.addr, align 8
  ret void
}

; Function Attrs: inlinehint ssp uwtable
define linkonce_odr void @_ZN7DerivedD2Ev(%struct.Derived* %this) unnamed_addr #2 align 2 {
entry:
  %this.addr = alloca %struct.Derived*, align 8
  store %struct.Derived* %this, %struct.Derived** %this.addr, align 8
  %this1 = load %struct.Derived*, %struct.Derived** %this.addr, align 8
  %0 = bitcast %struct.Derived* %this1 to %struct.Base*
  call void @_ZN4BaseD2Ev(%struct.Base* %0)
  ret void
}

; Function Attrs: ssp uwtable
define linkonce_odr void @_ZN4BaseD2Ev(%struct.Base* %this) unnamed_addr #0 align 2 {
entry:
  %this.addr = alloca %struct.Base*, align 8
  store %struct.Base* %this, %struct.Base** %this.addr, align 8
  %this1 = load %struct.Base*, %struct.Base** %this.addr, align 8
  %0 = bitcast %struct.Base* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV4Base, i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 8
  %1 = bitcast %struct.Base* %this1 to void (%struct.Base*)***
  %vtable = load void (%struct.Base*)**, void (%struct.Base*)*** %1, align 8
  %vfn = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable, i64 0
  %2 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn, align 8
  call void %2(%struct.Base* %this1)
  %3 = bitcast %struct.Base* %this1 to void (%struct.Base*)***
  %vtable2 = load void (%struct.Base*)**, void (%struct.Base*)*** %3, align 8
  %vfn3 = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable2, i64 2
  %4 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn3, align 8
  call void %4(%struct.Base* %this1)
  %5 = bitcast %struct.Base* %this1 to void (%struct.Base*)***
  %vtable4 = load void (%struct.Base*)**, void (%struct.Base*)*** %5, align 8
  %vfn5 = getelementptr inbounds void (%struct.Base*)*, void (%struct.Base*)** %vtable4, i64 1
  %6 = load void (%struct.Base*)*, void (%struct.Base*)** %vfn5, align 8
  call void %6(%struct.Base* %this1)
  ret void
}

attributes #0 = { ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { norecurse ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline noreturn nounwind }
attributes #4 = { nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn nounwind }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 4.0.0 (git@github.com:asutton/clang-reflect.git 8317e40b61d41300797ead958d7b7c9a13bed863) (git@github.com:llvm-mirror/llvm.git 650050c7c2061ec08da359b57e12ae7a4b8c8911)"}
