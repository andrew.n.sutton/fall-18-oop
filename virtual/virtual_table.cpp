
#include <iostream>

struct Base;
struct Derived;

void Base_f(Base* p);
void Base_h(Base* p);
void Derived_g(Derived* p);
void Derived_h(Derived* p);

// Defines the virtual table for the Base class.
//
// Normally, this is defined as an array, but array
// elements all have the same type, and that's not
// the case for virtual functions. We can make this
// a "typed" table, to reduce gross casting operations.
struct Base_vtable
{
  void (*f)(Base*) = &Base_f;
  void (*g)(Base*) = nullptr;
  void (*h)(Base*) = &Base_h;
};

// Defines the virtual table for the Derived class.
struct Derived_vtable
{
  void (*f)(Derived*) = reinterpret_cast<void (*)(Derived*)>(&Base_f);
  void (*g)(Derived*) = Derived_g;
  void (*h)(Derived*) = &Derived_h;
};

// Actual virtual table objects (these are just
// global variables).

Base_vtable base_vtbl;
Derived_vtable derived_vtbl;


struct Base
{
  Base()
  {
    vptr = &base_vtbl;
  }

  ~Base()
  {
    vptr = &base_vtbl;
  }

  void f() { get_vptr()->f(this); }
  void g() { get_vptr()->g(this); }
  void h() { get_vptr()->h(this); }

  Base_vtable* get_vptr() const { 
    return reinterpret_cast<Base_vtable*>(vptr);
  }

  // The virtual pointer for the polymorphic class.
  void* vptr;
};

void Base_f(Base* p) { 
  std::cout << "Base::f\n";
}

void Base_h(Base* p) {
  std::cout << "Base::h\n"; 
}

struct Derived : Base
{
  Derived()
  {
    vptr = &derived_vtbl;
  }

  ~Derived()
  { 
    vptr = &derived_vtbl;
  }

  int num = 42;
};

void Derived_g(Derived* this_) {
  std::cout << "Derived::g" << ' ' << this_->num << '\n';
}

void Derived_h(Derived* this_) { 
  std::cout << "Derived::h" << ' ' << this_->num << '\n'; 
}

void foo(Base& p) {
  p.f();
  p.g();
  p.h();
}

int main()
{
  Derived d;
  foo(d);
}
