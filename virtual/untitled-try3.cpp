
#include <iostream>

struct Base;
struct Derived;

void Base_f(Base* p);
void Base_h(Base* p);
void Derived_g(Base* p);
void Derived_h(Base* p);

struct Base
{
  Base()
  {
    f();
    // g(); // Will crash ("pure virtual")
    h(); // Prints Base::h
  }

  ~Base()
  {
    g_ptr = nullptr;
    h_ptr = &Base_h;

    f(); // calls Base::f
    h(); // call Base::h
    g(); // Crashes
  }

  void f() { f_ptr(this); }
  void g() { g_ptr(this); }
  void h() { h_ptr(this); }

  void (*f_ptr)(Base*) = &Base_f;
  void (*g_ptr)(Base*) = nullptr;
  void (*h_ptr)(Base*) = &Base_h;
};

void Base_f(Base* p) { std::cout << "B\n"; }
void Base_h(Base* p) { std::cout << "B::h\n"; }

struct Derived : Base
{
  Derived()
  {
    g_ptr = &Derived_g;
    h_ptr = &Derived_h;
  }

  ~Derived()
  { }

  int num = 42;
};

void Derived_g(Base* p)
{
  Derived* self = static_cast<Derived*>(p);
  std::cout << "D " << self->num << '\n';
}

void Derived_h(Base* p) { std::cout << "D::h\n"; }


void foo(Base& p) {
  p.f();
  p.g();
}

int main()
{
  Derived d;
  foo(d);
}
