
#include <algorithm>

int main()
{
  int arr[5] = {5, 4, 3, 2, 1};

  // In non-optimized mode this will crash.
  // In optimized, the compiler will rely
  // on the fact that it invokes undefined
  // behavior and "undo" the entire program.
  std::fill(arr, arr + 10, 0);

  return arr[0] + 1;
};
