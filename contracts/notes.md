
## Wide contracts

Accepts (nearly) anything as an input.

Has defined behavior for all inputs. A function
with a wide contract has a documented result for
any inputs that it's given. It could:

- return an error code
- set an error bit or flag (e.g., iostreams)
- throw an exception

The result is always detectable by the immediate 
caller of the function.

The result must always be checked by the caller
of the function, but not necessarily the immediate
caller (e.g., as with functions).

Failure to check the error condition of a function
with wide contracts AFTER THE CALL *will* cause 
bugs in your program. 
Assuming the output is valid causes your
program to continue running on faulty or invalid
assumptions (purely a logical error).

This is a security issue (relying on invalid data 
can lead to arbitrary code execution).

```cpp
template<typename T>
T& vector<T>::at(int n)
{
  if (!(0 <= n && n < size()))
    throw std::runtime_error("bounds error");
  return *(m_first + n);
}
```


## Narrow contracts

Accept only a limited set of valid inputs.

Behavior is undefined for invalid inputs. Anything can 
happen if behavior is not defined.

For example, this function has a narrow contract:
```cpp
template<typename T>
T& vector<T>::operator[](int n)
{
  return *(m_first + n);
}
```
The precondition is `0 <= n && n < size()`.

```cpp
vector<string> vec {"a", "b", "c"};
vec[5] = "garbage";
```

Failure to validate preconditions BEFORE the
call will lead to serious bugs (or weird
compiler optimizations).

There are no error conditions to check on
functions with narrow contracts.


