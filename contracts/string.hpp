#pragma once

// Include this header for the assert macro.
#include <cassert>

// Include this for std::strlen and std::strcpy.
#include <cstring>

class string
{
public:
  string(const char* ptr);
  ~string();

  int size() const;

  char& operator[](int n);

private:
  int m_len;
  char* m_ptr;
};

string::string(const char* ptr)
  : m_len(std::strlen(ptr)),
    m_ptr(new char[m_len + 1])
{
  std::strcpy(m_ptr, ptr);
}

string::~string()
{
  delete [] m_ptr;
}

/// Returns the size (length) of the string.
///
/// ensures: return >= 0.
int
string::size() const
{
  return m_len;
}

// Returns the nth character in the string.
//
// expects: 0 <= n && n < size()
inline char&
string::operator[](int n)
  [[expects: 0 <= n && n < size()]]
  [[ensures: ...]]
{
  // This is a mandatory check. It's redundant
  // and always true for code that satisfies
  // the precondition above.
  //
  // Don't do this for narrow contracts.
  // if (!(0 <= n && n < size()))
  //   throw std::runtime_error();

  // An assertion can be used to check contracts
  // in certain compilation modes. 
  assert(0 <= n && n < size());
  // Note that assert is a preprocessor *macro*.
  // It expands to this (more or less).
  //
  // if (!(0 <= n && n < size()))
  //   std::abort();
  //
  // In optimized compilation modes, asserts
  // turn into this:
  //
  //    (void)0;

  [[assert: 0 <= n && n < size()]];

  // Defensive programming. Any code that you
  // write that helps users of your code find
  // and fix their own logic errors.

  return *(m_ptr + n);
}

