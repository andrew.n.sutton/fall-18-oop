
#pragma once

double my_sqrt(double n);
/// Returns the square root of n.
///
/// precondition: n >= 0.
///
/// postcondition: r = my_sqrt(n); r * r == n;
///
/// Contract -- is a set of conditions that must
/// satisfied in order for the function to be
/// correct and used correctly.
///
/// Two sides: 
///
/// - client side (preconditions) must be
/// satisfied before calling the function.
///
/// - provider side (postconditions) must be
/// satisfied by the implementation effects
/// and return value.
///
/// Wide contract allows (almost) any input,
/// valid or not. For invalid inputs, we guarantee
/// particular behaviors. Set an error code,
/// throw an exception.
///
/// Narrow contract allows for only valid input.
/// What happens if you get invalid input?



