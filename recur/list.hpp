#pragma once

#include <string>

/// Represents the set of all nodes in
/// a linked list. There are two (so far?):
/// The empty node (or null), and a nonempty
/// node that contains a value.
class node
{
public:
  enum kind { empty, integer, string };

  node(kind k) : m_kind(k) { }

  kind get_kind() const { return m_kind; }

private:
  kind m_kind;
};

/// Represents empty (or null nodes), which
/// terminate linked lists.
class empty_node : public node
{
public:
  empty_node() : node(empty) { }
};

/// A list node that stores an integer value.
class int_node : public node
{
public:
  int_node(int n, node* p)
    : node(integer), m_value(n), m_next(p)
  { }

  node const* get_next() const { return m_next; }
  
  node* get_next() { return m_next; }

private:
  int m_value;
  /// The value stored in the node.

  node* m_next;
  /// The next node in the list. Defined
  /// recursively.
};


/// A list node that stores an integer value.
class string_node : public node
{
public:
  string_node(std::string const& s, node* p)
    : node(string), m_value(s), m_next(p)
  { }

  node const* get_next() const { return m_next; }
  
  node* get_next() { return m_next; }

private:
  std::string m_value;
  /// The value stored in the node.

  node* m_next;
  /// The next node in the list. Defined
  /// recursively.
};
