#pragma once

#include <string>
#include <iostream>

/// Represents the set of all nodes in
/// a linked list. There are two (so far?):
/// The empty node (or null), and a nonempty
/// node that contains a value.
class node
{
public:
  enum kind { empty, integer, string };

  node(kind k) : m_kind(k) { }

  kind get_kind() const { return m_kind; }

private:
  kind m_kind;
};

/// Represents empty (or null nodes), which
/// terminate linked lists.
class empty_node : public node
{
public:
  empty_node() : node(empty) { }
};


/// A list node that stores a value of some kind,
/// which is determined by derived classes.
class nonempty_node : public node
{
public:
  nonempty_node(kind k, node* p)
    : node(k), m_next(p)
  { }

  node const* get_next() const { return m_next; }
  
  node* get_next() { return m_next; }

private:
  node* m_next;
  /// The next node in the list. Defined
  /// recursively.
};


/// A list node that stores an integer value.
class int_node : public nonempty_node
{
public:
  int_node(int n, node* p)
    : nonempty_node(integer, p), m_value(n)
  { }

  int get_value() const { return m_value; }

private:
  int m_value;
  /// The value stored in the node.
};


/// A list node that stores an integer value.
class string_node : public nonempty_node
{
public:
  string_node(std::string const& s, node* p)
    : nonempty_node(string, p), m_value(s)
  {}

  std::string const& get_value() const { return m_value; }

private:
  std::string m_value;
  /// The value stored in the node.
};
