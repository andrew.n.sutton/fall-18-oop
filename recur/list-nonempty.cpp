
#include <iostream>

#include "list-nonempty.hpp"

/// Declares (but does not define) the "top-level"
/// size() function.
int size(node const* p);

int
empty_size(empty_node const* p)
{
  return 0;
}

int
nonempty_size(nonempty_node const* p)
{
  return 1 + size(p->get_next());
}

int 
size(node const* p)
{
  switch (p->get_kind()) {
  case node::empty:
    return empty_size(static_cast<empty_node const*>(p));
  default:
    return nonempty_size(static_cast<nonempty_node const*>(p));
  }
}


// print

/// Top-level function.
void print(node const* p);

void 
print(empty_node const* p)
{

}

void
print(int_node const* p)
{
  std::cout << p->get_value() << ' ';
  print(p->get_next());
}

void
print(string_node const* p)
{
  std::cout << p->get_value() << ' ';
  print(p->get_next());
}

void 
print(node const* p)
{
  switch (p->get_kind()) {
  case node::empty:
    return print(static_cast<empty_node const*>(p));
  case node::integer:
    return print(static_cast<int_node const*>(p));
  case node::string:
    return print(static_cast<string_node const*>(p));
  }
}

int
main()
{
  node* ptr = 
    new int_node(42,
      new string_node("hello",
        new int_node(5, 
          new empty_node())));

  std::cout << size(ptr) << '\n';

  print(ptr);
  std::cout << '\n';
}
