
#include <iostream>

#include "list.hpp"

/// Declares (but does not define) the "top-level"
/// size() function.
int size(node const* p);

int
empty_size(empty_node const* p)
{
  return 0;
}

int
int_size(int_node const* p)
{
  return 1 + size(p->get_next());
}

int
string_size(string_node const* p)
{
  return 1 + size(p->get_next());
}

int 
size(node const* p)
{
  switch (p->get_kind()) {
  case node::empty:
    return empty_size(static_cast<empty_node const*>(p));
  case node::integer:
    return int_size(static_cast<int_node const*>(p));
  case node::string:
    return string_size(static_cast<string_node const*>(p));
  }
}


int
main()
{
  node* ptr = 
    new int_node(42,
      new string_node("hello",
        new int_node(5, 
          new empty_node())));

  std::cout << size(ptr) << '\n';
}
