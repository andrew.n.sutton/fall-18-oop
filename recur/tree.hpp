#pragma once

/// Represents the set of all possible binary trees.
struct tree_node
{

};

/// Represents a leaf node in a binary tree.
struct leaf_node : public tree_node
{

};

/// Represents an interior node in binary tree.
struct interior_node : public tree_node
{
  tree_node* left;
  tree_node* right;
};
