
#include <iostream>

#include "list-poly.hpp"


int 
size(node const* p)
{
  return p->size();
}


void 
print(node const* p)
{
  p->print();
}

int
main()
{
  node* ptr = 
    new int_node(42,
      new string_node("hello",
        new int_node(5, 
          new empty_node())));

  std::cout << size(ptr) << '\n';

  print(ptr);
  std::cout << '\n';

  delete ptr;
}
