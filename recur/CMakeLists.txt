
set(CMAKE_CXX_FLAGS "-std=c++14")

add_executable(test-list list.cpp)
add_executable(test-list-nonempty list-nonempty.cpp)
add_executable(test-list-poly list-poly.cpp)