#pragma once

#include <string>
#include <iostream>

/// Represents the set of all nodes in
/// a linked list. There are two (so far?):
/// The empty node (or null), and a nonempty
/// node that contains a value.
class node
{
public:
  virtual ~node() = default;
  // Destroys the node.

  // Declares size to be a virtual function.
  // Calling a virtual function will dynamically
  // dispatch to call an overridden function.
  // See below.
  virtual int size() const { return -1; }

  virtual void print() const { }
};

/// Represents empty (or null nodes), which
/// terminate linked lists.
class empty_node : public node
{
public:
  empty_node() : node() { }

  // This function overrides the virtual size()
  // function in the base class.
  //
  // We are explicitly associating behavior with
  // objects of THIS class.
  int size() const override { return 0; }

  void print() const override { 
    std::cout << "<end>";
  }
};


/// A list node that stores a value of some kind,
/// which is determined by derived classes.
class nonempty_node : public node
{
public:
  nonempty_node(node* p)
    : node(), m_next(p)
  { }

  ~nonempty_node() {
    delete m_next;
  }

  node const* get_next() const { return m_next; }
  
  node* get_next() { return m_next; }

  // This function overrides the virtual size()
  // function in the base class.
  //
  // We are explicitly associating behavior with
  // objects of THIS class.
  int size() const override { return 1 + m_next->size(); }

private:
  node* m_next;
  /// The next node in the list. Defined
  /// recursively.
};


/// A list node that stores an integer value.
class int_node : public nonempty_node
{
public:
  int_node(int n, node* p)
    : nonempty_node(p), m_value(n)
  { }

  int get_value() const { return m_value; }

  void print() const override { 
    std::cout << m_value << ' '; 
    get_next()->print();
  }

private:
  int m_value;
  /// The value stored in the node.
};


/// A list node that stores an integer value.
class string_node : public nonempty_node
{
public:
  string_node(std::string const& s, node* p)
    : nonempty_node(p), m_value(s)
  {}

  std::string const& get_value() const { return m_value; }

  void print() const override { 
    std::cout << m_value << ' '; 
    get_next()->print();
  }

private:
  std::string m_value;
  /// The value stored in the node.
};
