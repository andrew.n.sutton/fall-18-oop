
// Visitor pattern.
//
// Allows polymorphic algorithms to be defined
// separately from the polymorphic classes that
// they operate on.

class ConstVisitor
{
  virtual void visit(Null const& val) = 0;
  virtual void visit(Bool const& val) = 0;
  virtual void visit(Number const& val) = 0;
  virtual void visit(String const& val) = 0;
  virtual void visit(Array const& val) = 0;
  virtual void visit(Object const& val) = 0;
};


/// Represents the set of all JSON values.
struct Value
{
  virtual ~Value() = default;
  /// Ensures proper cleanup.

  /// Creates a copy of `val`.
  ///
  /// Idiom/pattern: virtual constructor.
  virtual Value* clone() const = 0;

  virtual void accept(ConstVisitor& vis) const = 0;
  /// "Accepts" a visitor. Dispatches to the appropriate
  /// behavior in the visitor class.

  /// Reads from the stream `is`.
  static Value* read(std::istream&);
};

void write(Value const* val, std::ostream& os);

std::size_t hash(Value const* val);

bool equal(Value const* a, Value const* b);





/// Represents the value null.
struct Null : Value
{
  Value* clone() const override { return new Null(*this); }

  void accept(ConstVisitor& vis) const override { vis.visit(*this); }
};

/// Represents the values true and false.
struct Bool : Value
{
  Value* clone() const override { return new Bool(*this); }
  
  void accept(ConstVisitor& vis) const override { vis.visit(*this); }

  bool value;
};


/// Represents values of the form "...".
struct String : Value, std::string
{
  Value* clone() const override { return new String(*this); }

  void accept(ConstVisitor& vis) const override { vis.visit(*this); }
};

struct Array : Value, std::vector<Value*>
{
  /// Copies all elements of x into distinct array
  /// objects in this array (i.e., no sharing). Sometimes
  /// called a deep copy.
  ///
  /// Beware of "shallow" copies (results in shared data).
  Array(Array const& x) 
    : Value(), std::vector<Value*>()
  {
    reserve(x.size());
    for (Value* p : x)
      push_back(p->clone());
  }
  
  ~Array() override {
    for (Value* p : *this)
      delete p;
  }

  Value* clone() const override { 
    return new Array(*this);
  }

  void accept(ConstVisitor& vis) const override { vis.visit(*this); }
};

struct Object : Value, std::map<std::string, Value*>
{
  ~Object() override {
    for (auto const& kv : *this)
      delete p.second;
  }

  Value* clone() const override { 
  }

  void accept(ConstVisitor& vis) const override { vis.visit(*this); }
};

