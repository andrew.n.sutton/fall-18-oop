

/// Represents the set of all JSON values.
struct Value
{
  virtual ~Value() = default;
  /// Ensures proper cleanup.

  /// Creates a copy of `val`.
  ///
  /// Idiom/pattern: virtual constructor.
  virtual Value* clone() const = 0;

  /// Writes to the stream `is`.
  virtual void write(std::ostream& is) const = 0;

  /// Return a hash code for the value.
  virtual std::size_t hash() const = 0;

  /// Returns true if this and that are the same value.
  virtual bool equal(Value* val) const;

  /// Reads from the stream `is`.
  static Value* read(std::istream&);
};

/// Represents the value null.
struct Null : Value
{
  Value* clone() const override { return new Null(*this); }
};

/// Represents the values true and false.
struct Bool : Value
{
  Value* clone() const override { return new Bool(*this); }
  
  bool value;
};


/// Represents values of the form "...".
struct String : Value, std::string
{
  Value* clone() const override { return new String(*this); }
};

struct Array : Value, std::vector<Value*>
{
  /// Copies all elements of x into distinct array
  /// objects in this array (i.e., no sharing). Sometimes
  /// called a deep copy.
  ///
  /// Beware of "shallow" copies (results in shared data).
  Array(Array const& x) 
    : Value(), std::vector<Value*>()
  {
    reserve(x.size());
    for (Value* p : x)
      push_back(p->clone());
  }
  
  ~Array() override {
    for (Value* p : *this)
      delete p;
  }

  Value* clone() const override { 
    return new Array(*this);
  }
};

struct Object : Value, std::map<std::string, Value*>
{
  ~Object() override {
    for (auto const& kv : *this)
      delete p.second;
  }

  Value* clone() const override { 
  }
};

