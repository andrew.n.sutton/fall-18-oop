#include "json-vis.hpp"

#include <cassert>

// Not great. Linear sequence of ifs.
#if 0
void
write(Value const* val, std::ostream& os)
{
  if (dynamic_cast<Null>(val)) 
    os << "null";
  else if (Bool const* b = dynamic_cast<Bool>(val)) 
    os << (b->value ? "true" : "false");
  else if (Number const* p = dynamic_cast<Number>(val))
    os << p->value;
  else if (String const* p = dynamic_cast<String>(val))
    os << *p;
  else if (Array const* p = dynamic_cast<Array>(val))
    os << "array"; // FIXME: Implement me.
  else if (Object const* p = dynamic_cast<Object>(val))
    os << "object"; // FIXME: Implement me.
  else 
    assert(false);
}
#endif

// Basically what we want to do.
// This is not real code. Don't try writing it.
#if 0
void
write(Value const* val, std::ostream& os)
{
  switch virtual (val) {
  case Null const* x: os << "null";
  case Bool const* b: os << b->value;
  case Number const* n: os << "n->value";
  case String const* s: os << *s;
  case Array const* a: os << "array"; // FIXME: Implement me.
  case Object const* a: os << "object"; // FIXME: Implement me.
  }
}
#endif

  class PrintVisitor : public Visitor
  {
    std::ostream& os;

    PrintVisitor(std::ostream& os) : os(os) { }

    void visit(Null const& val) {
      os << "null";
    }
    void visit(Bool const& val) {
      os << (val.value ? "true" : "false");
    }
    void visit(Number const& val) {
      os << val.value;
    }
    void visit(String const& val) {
      os << *val;
    }
    void visit(Array const& val) {
      os << "[";
      for (Value* p : val)
        write(p, os);
      os << "]";
    }
    void visit(Object const& val) {
      os << "object"; // FIXME
    }
  };

void
write(Value const* val, std::ostream& os)
{
  PrintVisitor vis(os);
  val->accept(vis);
}

class HashVisitor : public Visitor
{
  // "Return values" are declared as members and store
  // the results of a computation.
  std::size_t ret;
  
  void visit(Null const& val) {
    ret = 0;
  }
  void visit(Bool const& val) {
    ret = 1;
  }
  void visit(Number const& val) {
    ret = 2;
  }
  void visit(String const& val) {
    ret = 3;
  }
  void visit(Array const& val) {
    ret = 4;
  }
  void visit(Object const& val) {
    ret = 5;
  }
};


std::size_t
hash(Value const* val)
{
  // Apply the visitor.
  HashVisitor vis;
  val->accept(vis);

  // Extract and return the hash key.
  return val->ret;
}

bool
equal(Value const* a, Value const* b)
{

}
