
#include <cstring>
#include <algorithm>
#include <iostream>

struct string
{
  int len;
  char* ptr;

  // And a bunch of other stuff.

  // This is a constructor
  string(char const* s)
    : len(std::strlen(s)), // These are member 
      ptr(new char[len])   // initializers
  { 
    std::copy(s, s + len, ptr);
  }

  char* begin() { return ptr; }
  char* end() { return ptr + len; }

  char const* begin() const { return ptr; }
  char const* end() const { return ptr + len; }
};

std::ostream&
operator<<(std::ostream& os, string const& str)
{
  for (char c : str)
    os << c;

  // The loop above is equivalent to this (more or less).
  //
  // for (auto iter = str.begin(); iter != str.end(); ++iter) {
  //   char c = *iter;
  //   os << c;
  // }

  return os;
}

int main() {
  // ... that makes this work.
  string s = "hello";
  s.len = 100000;
  std::cout << s << '\n';
}
