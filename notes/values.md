
Objects types and values...

What's a value?

Two kinds of values:
- abstract values -- anything that exists as a result of
some mathematical theory. Can you touch it? You can't
consume it either.

- concrete values -- almost everything else.

- mechanisms and facilities -- other stuff.

Values are not about computers. Values are things
in the universe.



What's an object?

A region of storage. Objects live in computer memory.
Objects have an address and a size. Objects are a
sequence of bytes (which also means bits).

Objects are created with a type (mostly).

Objects hold values -- holds a pattern of bits that
can be interpreted as a value.


what's a type?

Mostly its a description of how values are represented
in computer memory. It also gives us a way of interpreting
memory as values.

Languages give us tools for creating new types (i.e.,
classes) in order to build new abstractions, prevent 
misused, and for fun and profit.

-----

Kinds of problems to solve.

1. Enumerating a set of values (`enum`)
2. Aggregating sets of other values:
   (and -- class/struct, tuples)
3. Alternating sets of other values 
   (or -- union, class hierarchies/inheritance, variant)
4. Extending sets of values
   (class hierarchies/inheritance, templates)
5. Restricting sets of values
   (constructors)
2. Repeating sets of values (array, `vector`, `list`)

A language's *type system* provides tools for solving
these kinds of representation problems. Types are
used to build abstractions.

