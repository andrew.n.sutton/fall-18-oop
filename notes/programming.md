# Notes on programming

Programming (effective) requires simultaneously thinking
about three different, often orthogonal aspects of software:

1. Productivity
2. Safety
3. Efficiency

Productivity entails any programming or design choices
made to enable other programmers (including you) able to
be productive in a code base (find bugs, fix bugs, adding
features, new features).

We typically rely on *abstraction* to facilitate productivity.
Abstraction entails hiding details; building software to
reflect the entities of *your* problem (e.g., Employees,
Monsters, Web requests, Tasks) instead of the machine's
problems (pointers, instructions) or your languages problems
(template metaprogramming).

Classes are the main abstraction facility.

Safety covers any programming or design choices made to
reduce the number of logic errors committed by programmers
using your software (including yourself).

Abstraction facilitates safety.

```cpp
int a[] = { 1, 2, 3 };
int b[] = { 1, 2, 3 };
// sort(a + 3, a); // error
// sort (a, b) // error
// sort (b, a) // error

sort(a);
```

Encapsulation also facilitates safety. Encapsulation
is all about preventing access to implementation details.
Usually member access (public and private).

Efficiency is about making programs use as few resources
as possible (both time and memory).

Abstraction is often the enemy of efficiency.

