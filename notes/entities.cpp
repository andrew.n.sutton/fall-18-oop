// Hello world

#include <iostream>

// Declares a function (a thing) called
// main that returns an int and accepts
// two arguments: an integer and an unbound
// array of C-strings.
int main(int argc, char* argv[]);

// Defines the function main.
int main(int argc, char* argv[])
{
  return 0;
}

// Variables.

// Most variables are declarations and
// definitions at the same time.
//
// Declares x to be an object of type
// int with an initial value of 0.
int x = 0;

// Declares var to be an object of type
// int, but does not define it.
extern int var;

// What is this?
// p is an object; it is pointer to an 
// int object.
int* p = nullptr;

// What is this?
// r is a reference (not an object) to 
// an int object.
int& r = *p;



// Declares Some_class to be a class.
class Some_class;

// Defines Some_class to be a class.
// Classes are not objects or functions;
// they are descriptions of objects to
// be created later.
class Some_class {
  // Members and stuff.
};

// Defines an enum E. A, B, and C are
// E's enumerators.
//
// Enum is a kind of type.
enum Some_enum { A, B, C };

// Declares an object e that holds a
// value of type Some_enum and is 
// initialized to the value A.
Some_enum e = A;

// Defines std to be a namespace.
namespace std
{

}

// More kinds of things...

int f(int n) 
{
  // n != 0 is an expression
  // n is an expression
  // 0 is an expression
  //
  // Expressions compute values.
  // There is is a type that describes
  // those values (expressions have
  // a type).
  //
  while (n != 0) {
    std::cout << n << '\n';

    n != 0 && n > 95;

    // --n is an expression.
    //
    // Some expressions also side effects.
    --n;
  }
  return n;
}

